using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodToTempo : MonoBehaviour
{
    public MusicDataReader MusicDataReader;
    public VideoManager VideoManager;
    public RPCManager RPCManager;
    public Animator GuitaristAnimator;
    public TempoController TempoController;

    public bool isStart = false;
    public float speed = 1.0f;
    public float GlobalTempo;

    [SerializeField]
    private GameManager gameManager;
    [SerializeField]
    private float sensitivity = 0.05f;
    //[SerializeField]
    //private int minimumTempoChange = 10;

    //private bool isNodding = false;
    [SerializeField]
    private float noddingTerm;
    //private int noddingFrame = 20;
    //private float maxNodding = 30f;
    //private bool isTempo = false;
    private int isGuitarPlayingHash;
    private int isNoddingHash;
    private int index = 0;

    [SerializeField]
    private float diff;
    [SerializeField]
    private float oridiff;
    [SerializeField]
    private float targetDiff;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();

        isGuitarPlayingHash = Animator.StringToHash("isGuitarPlaying");
        isNoddingHash = Animator.StringToHash("isNodding");
    }


    // Update is called once per frame
    void Update()
    {
        if (gameManager.IsSongPlaying)
        {
            NodToCurrentTempo();
        }
    }


    void NodToCurrentTempo()
    {
        if (MusicDataReader.MusicData.NoteInfoArray[index].offset < VideoManager.Video.time + 0.01f)
        {
            oridiff = MusicDataReader.MusicData.NoteInfoArray[index + 1].offset - MusicDataReader.MusicData.NoteInfoArray[index].offset;
            oridiff /= VideoManager.Video.playbackSpeed;

            diff = oridiff;

            if (TempoController.IsTempoAdjustable && TempoController.noddingTerm < 2 && TempoController.noddingTerm > 0.1f)
            {
                targetDiff = TempoController.noddingTerm;
                //Debug.Log("Target: " + targetDiff + " dd : " + diff);

                if (Mathf.Abs(diff - targetDiff) < 0.03f)
                {
                    //diff = targetDiff;
                }
                else
                {
                    diff = (diff > targetDiff) ? diff - sensitivity : diff + sensitivity;
                }

                //Debug.Log("diff: " + diff + " ori : " + oridiff);
                //VideoManager.ChangeVideoSpeed(VideoManager.Video.playbackSpeed * oridiff / diff);

                if (gameManager.IsLeader)
                {
                    RPCManager.ChangeTempo(VideoManager.Video.playbackSpeed * oridiff / diff);
                }
            }

            float tempo = 60 / diff;

            GuitaristAnimator.Play("Nodding", -1, 0f);
            

            GuitaristAnimator.speed = tempo / 120f;

            index++;
        }
    }



    //void NodToFixedTempo()
    //{
    //    if (!ControlTempo.IsTempoAdjustable || !GuitaristAnimator.GetCurrentAnimatorStateInfo(0).IsName("PlayingGuitar")) return;
    //    float animationState = GuitaristAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
    //    animationState -= (int)animationState;

    //    if ((animationState > 0.95f && animationState < 1.0f) || (animationState > 0.45f && animationState < 0.5f))
    //    {
    //        if (!isTempo) return;

    //        isTempo = false;

    //        if (Mathf.Abs(GlobalTempo - ControlTempo.TargetTempo) < minimumTempoChange)
    //        {
    //            GlobalTempo = ControlTempo.TargetTempo;
    //        }
    //        else
    //        {
    //            GlobalTempo = (GlobalTempo > ControlTempo.TargetTempo) ? GlobalTempo - minimumTempoChange : GlobalTempo + minimumTempoChange;
    //        }

    //        //VideoManager.Video.playbackSpeed = GlobalTempo / MusicDataReader.MusicData.Tempo;
    //        VideoManager.ChangeVideoSpeed(GlobalTempo / MusicDataReader.MusicData.Tempo);

    //        GuitaristAnimator.speed = (MusicDataReader.MusicData.Tempo * VideoManager.Video.playbackSpeed) / 120f;
    //    }
    //    else
    //    {
    //        isTempo = true;
    //    }
    //}
}
