using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempoController : MonoBehaviour
{
    //public MidiPlayerTK.MidiFilePlayer MidiFilePlayer;
    public MusicDataReader MusicDataReader;
    public GameManager GameManager;

    public float TargetTempo;
    public bool IsTempoAdjustable = false;
    public float noddingTerm;
    public GameObject NoteLane;

    private float previousAngle;
    private float previousAngularVelocity;
    private float previousTurningPoint;
    private float previousTime;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A) || (GameManager.IsMyGaze && GameManager.IsOtherGaze))
        {
            //NoteLane.transform.localPosition = new Vector3(-1.0f, -1.0f, 2.0f);
            IsTempoAdjustable = true;
        }
        else
        {
            //NoteLane.transform.localPosition = new Vector3(-1.0f, -1.0f, 2.0f);
            IsTempoAdjustable = false;
        }

        if (!GameManager.IsLeader) return;

        if (!IsTempoAdjustable) return;

        float adjustedAngle;
        float originalTempo = MusicDataReader.MusicData.Tempo;

        if (gameObject.transform.eulerAngles.x > 200f)
        {
            adjustedAngle = 360f - gameObject.transform.eulerAngles.x;
        }
        else
        {
            adjustedAngle = -gameObject.transform.eulerAngles.x;
        }
        
        float angularVelocity = adjustedAngle - previousAngle;

        if (angularVelocity * previousAngularVelocity < 0)
        {
            float turnigPoint = adjustedAngle;

            if (Mathf.Abs(turnigPoint - previousTurningPoint) > 10f && angularVelocity > 0)
            {
                float turningTime = Time.time;
                noddingTerm = turningTime - previousTime;

                // Control tempo by tempo
                int temporaryTempo = (int)(60 / noddingTerm);

                if (temporaryTempo > 30)
                {
                    Debug.Log("t: " + temporaryTempo);
                    TargetTempo = temporaryTempo;
                }

                previousTime = turningTime;
            }

            previousTurningPoint = turnigPoint;            
        }

        previousAngle = adjustedAngle;
        previousAngularVelocity = angularVelocity;
    }
}
