using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class EyecastDetection : MonoBehaviour
{
    [SerializeField]
    private Image alertImage;

    public RPCManager RPCManager;

    void Start()
    {
        alertImage.enabled = false;
    }

    public void EnableImage()
    {
        alertImage.enabled = true;
    }
    public void DisableImage()
    {
        alertImage.enabled = false;
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Human"))
        {
            RPCManager.EyeContact();
        }
    }
    void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.CompareTag("Human"))
        {
            RPCManager.EyeExit();
        }
    }
}
