using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RPCManager : MonoBehaviour
{
    [SerializeField]
    private GameManager gameManager;

    private NoteManager noteManager;
    private SoundManager soundManager;
    private VideoManager videoManager;
    private PhotonView photonView;
    private HitBar hitBar;
    private EyecastDetection eyecastDetection;

    public bool ISMe = false;

    public Transform Head_RPC;
    public Transform LookTarget_RPC;

    public Transform Head;
    public Transform LookTarget;

    public Transform OtherHead;
    public Transform OtherLookTarget;

    // Start is called before the first frame update
    void Start()
    {
        photonView = GetComponent<PhotonView>();

        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        videoManager = GameObject.Find("VideoManager").GetComponent<VideoManager>();
        noteManager = GameObject.Find("NoteManager").GetComponent<NoteManager>();
        hitBar = FindObjectOfType<HitBar>();
        eyecastDetection = GameObject.Find("Eyecast").GetComponent<EyecastDetection>();

        Head = GameObject.Find("CenterEyeAnchor").GetComponent<Transform>();
        LookTarget = GameObject.Find("PlayerLookTarget").GetComponent<Transform>(); ;

        if (photonView.IsMine)
        {
            soundManager.RPCManager = this;

            gameManager.RPCManager = this;

            hitBar.RPCManager = this;

            FindObjectOfType<NodToTempo>().RPCManager = this;

            eyecastDetection.RPCManager = this;
        }
        else 
        {
            OtherHead = GameObject.Find("OtherHead").GetComponent<Transform>();
            OtherLookTarget = GameObject.Find("OtherLookTarget").GetComponent<Transform>();
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine)
        {
            MapPosition(Head, Head_RPC);
            MapPosition(LookTarget, LookTarget_RPC);
        }
        else 
        {
            MapPosition(Head_RPC, OtherHead);
            MapPosition(LookTarget_RPC, OtherLookTarget);
        }
    }

    public void PressButton()
    {
        photonView.RPC("PressButton_RPC", RpcTarget.All);

    }

    public void PlaySound(float pitch, float volume)
    {
        photonView.RPC("PlaySound_RPC", RpcTarget.Others, pitch, gameManager.IsLeader, volume);
    }

    public void ChangeTempo(float speed)
    {
        photonView.RPC("ChangeTempo_RPC", RpcTarget.All, speed);
    }

    public void ChangeAllNoteSize(float size)
    {
        photonView.RPC("ChangeNoteSize_RPC", RpcTarget.All, 1.5f);
    }

    public void EyeContact()
    {
        photonView.RPC("EyeContact_RPC", RpcTarget.All);
    }

    public void EyeExit()
    {
        photonView.RPC("EyeExit_RPC", RpcTarget.All);
    }


    [PunRPC]
    public void PressButton_RPC()
    {
        Debug.Log("PressButton_RPC");

        if (photonView.IsMine)
        {
            Debug.Log("My Ready");
            gameManager.IsMyStart = true;
        }
        else
        {
            Debug.Log("Other Ready");
            gameManager.IsOtherStart = true;
        }
    }


    [PunRPC]
    public void PlaySound_RPC(float pitch, bool isLeader, float volume)
    {
        Debug.Log("Sound Play!");
        soundManager.Play(pitch, isLeader, volume);      
    }


    [PunRPC]
    public void ChangeTempo_RPC(float speed)
    {
        Debug.Log("Tempo Change!");
        videoManager.ChangeVideoSpeed(speed);
    }


    [PunRPC]
    public void ChangeNoteSize_RPC(float size)
    {
        Debug.Log("Note Size Change!: " + size);

        noteManager.ChangeNoteSize(size);
    }

    [PunRPC]
    public void EyeContact_RPC()
    {
        Debug.Log("Eye Contacted!");

        if (photonView.IsMine)
        {
            gameManager.IsMyGaze = true;
        }
        else
        {
            eyecastDetection.EnableImage();
            gameManager.IsOtherGaze = true;
        }
    }


    [PunRPC]
    public void EyeExit_RPC()
    {
        Debug.Log("Look Away!");

        if (photonView.IsMine)
        {
            gameManager.IsMyGaze = false;
        }
        else
        {
            eyecastDetection.DisableImage();
            gameManager.IsOtherGaze = false;
        }
    }

    private void MapPosition(Transform from, Transform to)
    {
        to.transform.position = from.transform.position;
        to.transform.rotation = from.transform.rotation;
    }
}
