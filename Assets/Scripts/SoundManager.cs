using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public GameManager GameManager;
    public NoteManager NoteManager;
    public GameObject SoundSource;
    public Queue<GameObject> SoundSourcePool = new Queue<GameObject>();
    public RPCManager RPCManager;
    public RightHandGestureController RightHandGestureController;

    [SerializeField]
    private float basePitch = 25;
    private int soundSourcePoolCount = 30;


    // Start is called before the first frame update
    public void OnStart()
    {
        GenerateNotePool();
    }


    void GenerateNotePool()
    {
        for (int i = 0; i < soundSourcePoolCount; i++)
        {
            SoundSourcePool.Enqueue(Instantiate<GameObject>(SoundSource, gameObject.transform));
        }
    }


    public void Play(float pitch, bool isLeader, float volume)
    {
        if (SoundSourcePool.Count < 0)
        {
            Debug.LogError("Sound source pool is empty");
            return;
        }
        StartCoroutine(MakeSound(pitch - basePitch, isLeader, volume));
    }


    private IEnumerator MakeSound(float pitch, bool isLeader, float volume)
    {
        GameObject soundSource = SoundSourcePool.Dequeue();
        AudioSource audioSource = soundSource.GetComponent<AudioSource>();
        audioSource.pitch = Mathf.Pow(2f, (pitch) / 12.0f);
        audioSource.volume = volume;
        //0.1f + 0.5f * RightHandGestureController.ControllerSpeed
        if (isLeader && (GameManager.IsMyGaze && GameManager.IsOtherGaze))
        {
            Debug.Log("change");
            NoteManager.ChangeNoteSize(volume);
            //RPCManager.ChangeAllNoteSize(audioSource.volume);
        }

        //Debug.Log("volume: " + audioSource.volume);
        audioSource.Play();
        yield return new WaitForSeconds(3.5f);
   
        SoundSourcePool.Enqueue(soundSource);
    }
    

    private void Update()
    {

    }
}
