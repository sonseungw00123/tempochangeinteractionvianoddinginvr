using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class GameManager : MonoBehaviour
{
    public VideoManager VideoManager;
    public MusicDataReader MusicDataReader;
    public NoteManager NoteManager;
    public SoundManager SoundManager;
    public TempoController TempoController;
    public RPCManager RPCManager;

    public GameMode CurrentGameMode;
    public GameLevel CurrentGameLevel;
    public Instrumnets CurrentInstrument;
    public Role PlayerRole;


    public GameObject StartImage;
    public GameObject PressText;
    public GameObject WaitText;

    public bool IsGameEnd = false;
    public bool IsSongPlaying = false;

    public bool IsMyStart = false;
    public bool IsOtherStart = false;

    public bool IsMyGaze = false;
    public bool IsOtherGaze = false;

    public bool IsLeader = false;
    public bool IsFollower = false;

    private int isGuitarPlayingHash;

    private static GameManager _instance;



    public enum Role
    {
        Leader,
        Follower
    }


    public enum GameMode
    {
        Basic,
        FakePlay
    }


    public enum GameLevel
    {
        None,
        Timing,
        Hand,
        Both
    }


    public enum Instrumnets
    {
        Bass,
        Guitar
    }


    public static GameManager Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType(typeof(GameManager)) as GameManager;

                if (_instance == null)
                    Debug.Log("no Singleton obj");
            }
            return _instance;
        }
    }


    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(GameLoop());
    }


    // Update is called once per frame
    void Update()
    {
        if (IsSongPlaying)
        {
            NoteManager.OnUpdate();
        }
    }


    private IEnumerator GameLoop()
    {
        yield return StartCoroutine(RoundSetting());

        yield return StartCoroutine(RoundPlaying());

        yield return StartCoroutine(RoundEnding());

        if (IsGameEnd)
        {
            Debug.Log("Game End");
        }
        else
        {
            StartCoroutine(GameLoop());
        }
    }



    private IEnumerator RoundSetting()
    {
        yield return new WaitForSeconds(0.5f);
        Debug.Log("Round Setting...");

        MusicDataReader.OnStart();

        yield return new WaitForSeconds(0.5f);

        SoundManager.OnStart();
        NoteManager.OnStart();
        TempoController.TargetTempo = MusicDataReader.MusicData.Tempo;

        Debug.Log("Round Setting Done");
        yield return new WaitForSeconds(0.5f); 
    }



    private IEnumerator RoundPlaying()
    {
        Debug.Log("Wait For Ready...");

        while (!IsMyStart && !IsOtherStart)
        {
            if (Input.GetKeyDown(KeyCode.A) || OVRInput.Get(OVRInput.Button.Two))
            {
                RPCManager.PressButton();
            }

            yield return null;
        }

        Debug.Log("Round Playing...");
        VideoManager.Video.Play();
        IsSongPlaying = true;


        while (VideoManager.Video.time < VideoManager.Video.length)
        {
            yield return null;
        }

        Debug.Log("Round Playing Done");
        IsSongPlaying = false;

        yield return new WaitForSeconds(1.0f);
    }



    private IEnumerator RoundEnding()
    {
        Debug.Log("Round Ending...");

        bool isKeyDown = false;

        while (!isKeyDown)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                isKeyDown = true;
                IsGameEnd = true;
            }
            else if (Input.anyKeyDown)
            {
                isKeyDown = true;
            }

            yield return null;
        }

        Debug.Log("Round Ending Done");
    }
}
