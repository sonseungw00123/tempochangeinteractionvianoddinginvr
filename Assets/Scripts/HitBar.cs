using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBar : MonoBehaviour
{
    public GameManager GameManager;
    public NoteManager NoteManager;
    public VideoManager VideoManager;
    public GuitarInteractionManager GuitarInteractionManager;
    public SoundManager SoundManager;
    public Animator GuitaristAnimator;
    public RPCManager RPCManager;
    public RightHandGestureController RightHandGestureController;

    public ParticleSystem RippleEffect;

    //private bool isNoteInHitBar = false;
    [SerializeField]
    private GameObject currentNote;
    [SerializeField]
    private float previousPitch;
    [SerializeField]
    private float currentPitch;
    [SerializeField]
    private int currentNoteIndexInNotePool;
    [SerializeField]
    private bool isPitchChanged;
    [SerializeField]
    private bool isHandMove = false;
    private bool isMissNote = false;
    private float animationSpeed;

    private bool isTriggerDown = false;


    public void InitializeNoteInfo()
    { 
        currentNoteIndexInNotePool = 0;
        currentPitch = NoteManager.NotePool[currentNoteIndexInNotePool].GetComponent<Note>().pitch;
        previousPitch = currentPitch;
        isPitchChanged = false;

        gameObject.GetComponent<Collider>().enabled = true;
    }


    public void PressHitBar()
    {
        if (isTriggerDown && OVRInput.Get(OVRInput.Axis1D.SecondaryIndexTrigger, OVRInput.Controller.Touch) < 0.5)
        {
            isTriggerDown = false;
        }

        if (Input.GetKeyDown(KeyCode.DownArrow) || 
            OVRInput.GetDown(OVRInput.Button.One) || 
            (!isTriggerDown && OVRInput.Get(OVRInput.Axis1D.SecondaryIndexTrigger, OVRInput.Controller.Touch) > 0.5))
        {
            isTriggerDown = true;
            RippleEffect.Play();

            isHandMove = GuitarInteractionManager.CheckChordChange();

            float volume = 0.1f + 0.5f * RightHandGestureController.ControllerSpeed;

            switch (GameManager.CurrentGameLevel)
            {
                case GameManager.GameLevel.None:
                    Debug.Log("Good");

                    if (GameManager.CurrentInstrument == GameManager.Instrumnets.Guitar)
                    {
                        SoundManager.Play(currentPitch + 12, GameManager.IsLeader, volume);
                        RPCManager.PlaySound(currentPitch + 12, volume);
                    }
                    else
                    {
                        SoundManager.Play(currentPitch, GameManager.IsLeader, volume);
                        RPCManager.PlaySound(currentPitch, volume);
                    }

                    if (currentNote != null)
                    {
                        NoteManager.SetNoteOnProperPosition(currentNote);
                        UpdateNoteInfo();

                        currentNote = null;

                        if (VideoManager.Video.isPaused)
                        {
                            VideoManager.Video.Play();

                            GuitaristAnimator.speed = animationSpeed;
                        }
                    }

                    break;

                case GameManager.GameLevel.Hand:

                    if (!isMissNote && (isPitchChanged ^ isHandMove))
                    {
                        // Make wrong sound
                        Debug.Log("Miss");
                        if (GameManager.CurrentInstrument == GameManager.Instrumnets.Guitar)
                        {
                            SoundManager.Play(currentPitch + 15, GameManager.IsLeader, volume);
                            RPCManager.PlaySound(currentPitch + 15, volume);
                        }
                        else
                        {
                            SoundManager.Play(currentPitch + 3, GameManager.IsLeader, volume);
                            RPCManager.PlaySound(currentPitch + 3, volume);
                        }
                    }
                    // Pitch changed and hand move or pitch unchanged and hand not move, or miss previous note
                    else
                    {
                        // Make proper sound
                        Debug.Log("Good");

                        if (GameManager.CurrentInstrument == GameManager.Instrumnets.Guitar)
                        {
                            SoundManager.Play(currentPitch + 12, GameManager.IsLeader, volume);
                            RPCManager.PlaySound(currentPitch + 12, volume);
                        }
                        else
                        {
                            SoundManager.Play(currentPitch, GameManager.IsLeader, volume);
                            RPCManager.PlaySound(currentPitch, volume);
                        }
                    }

                    isPitchChanged = false;

                    if (currentNote != null)
                    {
                        NoteManager.SetNoteOnProperPosition(currentNote);
                        UpdateNoteInfo();

                        currentNote = null;

                        if (VideoManager.Video.isPaused)
                        {
                            VideoManager.Video.Play();
                            GuitaristAnimator.speed = animationSpeed;
                        }
                    }

                    isMissNote = false;

                    break;

                case GameManager.GameLevel.Timing:
                    if (currentNote != null)
                    {
                        Debug.Log("Good");
                        SoundManager.Play(currentPitch, GameManager.IsLeader, volume);
                        RPCManager.PlaySound(currentPitch, volume);

                        NoteManager.SetNoteOnProperPosition(currentNote);
                        UpdateNoteInfo();

                        currentNote = null;

                        if (VideoManager.Video.isPaused)
                        {
                            VideoManager.Video.Play();
                            GuitaristAnimator.speed = animationSpeed;
                        }
                    }
                    break;

                case GameManager.GameLevel.Both:
                    if (currentNote != null)
                    {
                        // Pitch changed but hand not move or pitch unchanged but hand move
                        if (!isMissNote && (isPitchChanged ^ isHandMove))
                        {
                            // Make wrong sound
                            Debug.Log("Miss");
                            SoundManager.Play(currentPitch + 3, GameManager.IsLeader, volume);
                            RPCManager.PlaySound(currentPitch + 3, volume);
                        }
                        // Pitch changed and hand move or pitch unchanged and hand not move, or miss previous note
                        else
                        {
                            // Make proper sound
                            Debug.Log("Good");
                            SoundManager.Play(currentPitch, GameManager.IsLeader, volume);
                            RPCManager.PlaySound(currentPitch, volume);
                        }

                        NoteManager.SetNoteOnProperPosition(currentNote);
                        UpdateNoteInfo();

                        currentNote = null;

                        if (VideoManager.Video.isPaused)
                        {
                            VideoManager.Video.Play();
                            GuitaristAnimator.speed = animationSpeed;
                        }
                    }

                    isMissNote = false;

                    break;
            }
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Note")
        {
            currentNote = other.gameObject;
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (currentNote != null && other.gameObject.tag == "Note")
        {
            // If Basic mode, then just set note on next position
            if (GameManager.CurrentGameMode == GameManager.GameMode.Basic)
            {
                NoteManager.SetNoteOnProperPosition(currentNote);
                UpdateNoteInfo();

                isMissNote = true;
                currentNote = null;
            }
            // If Fake Play mode, then waiting input
            else if (GameManager.CurrentGameMode == GameManager.GameMode.FakePlay)
            { 
                VideoManager.Video.Pause();
                animationSpeed = GuitaristAnimator.speed;
                GuitaristAnimator.speed = 0;
            }
        }
    }


    private void UpdateNoteInfo()
    {
        currentNoteIndexInNotePool++;
        
        if (currentNoteIndexInNotePool == NoteManager.NotePool.Count)
        {
            currentNoteIndexInNotePool = 0;
        }

        previousPitch = currentPitch;
        currentPitch = NoteManager.NotePool[currentNoteIndexInNotePool].GetComponent<Note>().pitch;

        if (previousPitch != currentPitch)
        {
            isPitchChanged = true;
        }
        else 
        {
            isPitchChanged = false;
        }
    }
}
