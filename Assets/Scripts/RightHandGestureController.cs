using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightHandGestureController : MonoBehaviour
{
    public float ControllerSpeed = 0.0f;
    private Vector3 previousPosition;
    private float sensitivity = 0.7f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float deltaSpeed = (transform.position - previousPosition).magnitude / Time.deltaTime;

        ControllerSpeed = ControllerSpeed * (1 - sensitivity) + deltaSpeed * sensitivity;

        previousPosition = transform.position;
    }
}
