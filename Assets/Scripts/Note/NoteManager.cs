using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteManager : MonoBehaviour
{
    public MusicDataReader MusicDataReader;
    public StartPoint StartPoint;
    public HitBar HitBar;
    public float NoteInterval = 10.0f;
    public List<GameObject> NotePool = new List<GameObject>();
    public Queue<NoteInfo> NoteInfoQueue = new Queue<NoteInfo>();
    public GameObject Note;

    private int notePoolCount = 30;
    private float previousPitch;
    private Color currentColor = Color.red;
    private float noteSize = 1.0f;

    // Start is called before the first frame update
    public void OnStart()
    {
        GenerateNotePool();
        CreateNoteQueue(MusicDataReader.AdjustedNoteInfoArray);
        InitializeNotes();
    }

    // Update is called once per frame
    public void OnUpdate()
    {
        StartPoint.MoveStartPointToVideoSpeed();
        HitBar.PressHitBar();
    }


    public void CreateNoteQueue(List<NoteInfo> noteInfoList)
    {
        for (int i = 0; i < noteInfoList.Count; i++)
        {
            NoteInfoQueue.Enqueue(noteInfoList[i]);
        }
    }


    public void ChangeNoteSize(float size)
    {
        Debug.Log("change note size");
        for (int i = 0; i < notePoolCount; i++)
        {
            NotePool[i].transform.localScale = new Vector3(0.5f + 1.5f * size, 0.5f + 1.5f * size, 0.5f + 1.5f * size);
            noteSize = 0.5f + 1.5f * size;
        }
    }

        
    public void SetNoteOnProperPosition(GameObject Note)
    {
        if (NoteInfoQueue.Count > 0)
        {
            NoteInfo noteInfo = NoteInfoQueue.Dequeue();
            //Note.transform.localPosition = new Vector3(0.0f, -(noteInfo.offset / MusicDataReader.MusicData.Tempo * 60f) * NoteInterval, 0.0f);
            Note.transform.localPosition = new Vector3(0.0f, -(noteInfo.offset + 60f/70f) * NoteInterval, 0.0f);
            Note.GetComponent<Note>().pitch = noteInfo.pitch;
            Note.transform.localScale = new Vector3(noteSize, noteSize, noteSize);


            // Change the color of the Note, if the pitch of the Note is different from the previous pitch
            if (previousPitch != noteInfo.pitch)
            {
                ChangeNoteColor();
            }

            Note.GetComponent<Renderer>().material.color = currentColor;
            Note.GetComponent<Renderer>().material.SetColor("_EmissionColor", currentColor);

            previousPitch = noteInfo.pitch;
        }
        else
        {
            Note.gameObject.SetActive(false);
        }
    }

    private void ChangeNoteColor()
    {
        if (currentColor == Color.blue)
        {
            currentColor = Color.red;
        }
        else
        {
            currentColor = Color.blue;
        }

    }

    private void InitializeNotes()
    {
        previousPitch = NoteInfoQueue.Peek().pitch;

        for (int i = 0; i < NotePool.Count; i++)
        {
            SetNoteOnProperPosition(NotePool[i]);
        }
    }
    private void GenerateNotePool()
    {
        for (int i = 0; i < notePoolCount; i++)
        {
            NotePool.Add(Instantiate<GameObject>(Note, StartPoint.gameObject.transform));
        }
    }
}
