using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLookTargetController : MonoBehaviour
{
    public GameObject Head;
    public GameObject LookTarget;

    public GameObject AvatarLookTarget;

    public Vector3 positionDifference;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        positionDifference = LookTarget.transform.localPosition - Head.transform.localPosition;

        AvatarLookTarget.transform.localPosition = positionDifference;
    }
}
